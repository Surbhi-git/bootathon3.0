function armfn() {
    let i = 1;
    let count = 0;
    document.getElementById("show").innerHTML = ("<b>Armstrong numbers between 100-999 are:  </b>");
    for (i = 100; i <= 999; i++) {
        var a = i;
        var s = 0;
        while (a > 0) {
            var b = a % 10;
            s = s + (b * b * b);
            a = Math.floor(a / 10);
        }
        if (s == i) {
            document.getElementById("show").innerHTML += (" " + s.toString());
        }
    }
}
//# sourceMappingURL=Armstrong.js.map