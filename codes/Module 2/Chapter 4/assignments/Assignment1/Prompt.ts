function promfn(){
    let a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let i:number;
    let num:number;
    var positive_count:number=0;
    var negative_count:number=0;
    var zero_count:number=0;

    var count:number=+a.value;
    if(isNaN(count)){
        alert("Please enter a valid number!");
    }
    else{
        for(i=0;i<count;i++){
            num=+prompt("Enter a number");
            if(isNaN(num)){
                alert( "Please enter a valid number!");
                i--;
            }
            else{
                if(num>0){
                    positive_count++;
                }
                else if(num<0){
                    negative_count++;
                }
                else{
                    zero_count++;
                }
            }
            
            
        }
        document.getElementById("show").innerHTML+=("<b>Number of positive numbers:  </b>")+positive_count;
        document.getElementById("show").innerHTML+=("<br><b>Number of negative numbers:  </b>")+negative_count;
        document.getElementById("show").innerHTML+=("<br><b>Number of zero numbers:  </b>")+zero_count;
    }

    
}